/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Dimension;
import javax.swing.JPanel;

/**
 *
 * @author gdeusdara
 */
public class TrianguloDePotencias  extends JPanel{
    
    private GraphTriangle triangulo;
    
    
    public TrianguloDePotencias(){
        construirPainel();
        
    }
    
    private void construirPainel(){
        triangulo = new GraphTriangle(0,0);
        triangulo.setPreferredSize(new Dimension(100,100));
        add(triangulo);
    }
    
    public void contruirTriangulo(double pontoX, double pontoY){
        triangulo.setCoordenadas(pontoX, pontoY);
    }
    
}
