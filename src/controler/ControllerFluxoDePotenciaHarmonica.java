/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import model.FluxoDePotenciaFundamental;
import view.Grafico;
import view.ModeloOnda;
import view.TelaFluxoDePotenciaFundamental;
import view.TrianguloDePotencias;

/**
 *
 * @author gdeusdara
 */
public class ControllerFluxoDePotenciaHarmonica {
    private FluxoDePotenciaFundamental modelo;
    private ModeloOnda tensao, corrente;
    
    public void simularActionPerformed(ActionEvent evt, Grafico grafico, JTextField potenciaAtiva, JTextField potenciaReativa, JTextField potenciaAparente, JTextField fatorDePotencia, TrianguloDePotencias triangulo) { 
        // TODO add your handling code here:
        modelo = new FluxoDePotenciaFundamental(tensao.getModelo(), corrente.getModelo());
        grafico.construirOnda(modelo.getOndaResultante());
        potenciaAtiva.setText(String.valueOf(modelo.getPotenciaAtiva()));
        potenciaReativa.setText(String.valueOf(modelo.getPotenciaReativa()));
        potenciaAparente.setText(String.valueOf(modelo.getPotenciaAparente()));
        fatorDePotencia.setText(String.valueOf(modelo.getfatorDePotencia()));
        double x = modelo.getPotenciaAtiva();
        double y = modelo.getPotenciaReativa();
        if (x > 5000 || y > 5000 || x < -5000 || y < -5000) {
            triangulo.contruirTriangulo(x / 250, y / 250);
        } else if (x > 1000 || y > 1000 || x < -1000 || y < -1000) {
            triangulo.contruirTriangulo(x / 100, y / 100);
        } else if (x > 500 || y > 500 || x < -500 || y < -500) {
            triangulo.contruirTriangulo(x / 10, y / 10);
        } else {
            triangulo.contruirTriangulo(x / 5, y / 5);
        }
    }

    public void sairActionPerformed(ActionEvent evt, TelaFluxoDePotenciaFundamental telaFluxoDePotenciaFundamental) {
        try {
            // TODO add your handling code here:
            telaFluxoDePotenciaFundamental.setVisible(false);
        } catch (Throwable ex) {
            Logger.getLogger(TelaFluxoDePotenciaFundamental.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resultadoPotenciaReativaActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    public void resultadoPotenciaAtivaActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }
    
}
