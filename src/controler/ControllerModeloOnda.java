/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import model.OndaComum;
import view.Grafico;

/**
 *
 * @author gdeusdara
 */
public class ControllerModeloOnda {
    private OndaComum modelo;

    public void anguloActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    public void sliderStateChanged(ChangeEvent evt, JTextField angulo, JSlider slider) {
        // TODO add your handling code here:
        angulo.setText(String.valueOf(slider.getValue()));
    }

    public void simularActionPerformed(ActionEvent evt, JTextField angulo, JTextField amplitude, Grafico grafico) {
        // TODO add your handling code here:
        modelo.setAmplitude(Double.parseDouble(amplitude.getText()));
        modelo.setAngulo(Double.parseDouble(angulo.getText()));
        grafico.construirOnda(modelo.getOnda());
    }
    
}
