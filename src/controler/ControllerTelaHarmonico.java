/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import model.Harmonico;
import view.Grafico;
import view.TelaHarmonico;

/**
 *
 * @author gdeusdara
 */
public class ControllerTelaHarmonico {
    private Harmonico modelo;                    
    private javax.swing.JTextField amplitude;
    private javax.swing.JTextField angulo;
    private view.Grafico grafico;
    private javax.swing.JSpinner ordemHarmonica;
    private javax.swing.JSlider sliderAmplitude;
    private javax.swing.JSlider sliderAngulo;
    
    /**
     *
     * @param amplitude
     * @param angulo
     * @param grafico
     * @param ordemHarmonica
     * @param sliderAmplitude
     * @param sliderAngulo
     */
    public ControllerTelaHarmonico(JTextField amplitude, JTextField angulo, Grafico grafico, JSpinner ordemHarmonica, JSlider sliderAmplitude, JSlider sliderAngulo){
    this.amplitude = amplitude;
    this.angulo = angulo;
    this.grafico = grafico;
    this.ordemHarmonica = ordemHarmonica;
    this.sliderAmplitude = sliderAmplitude;
    this.sliderAngulo = sliderAngulo;
    
    }

    public void sliderAnguloStateChanged(ChangeEvent evt) {
        // TODO add your handling code here:
        angulo.setText(String.valueOf(sliderAngulo.getValue()));
    }

    public void simularActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
        modelo.setAmplitude(Double.parseDouble(amplitude.getText()));
        modelo.setAngulo(Double.parseDouble(angulo.getText()));
        modelo.setOrdemHarmonica(ordemHarmonica.getValue().hashCode());
        grafico.construirOnda(modelo.getOnda());
    }

    public void sliderAmplitudeStateChanged(ChangeEvent evt, TelaHarmonico telaHarmonico) {
        // TODO add your handling code here:
        amplitude.setText(String.valueOf(sliderAmplitude.getValue()));
    }

    public void amplitudeActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }
}
