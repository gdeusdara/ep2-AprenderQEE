/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import model.DistorcaoHarmonica;
import model.FluxoDePotenciaFundamental;
import view.Grafico;
import view.ModeloOnda;
import view.TelaDistorcaoHarmonica;
import view.TelaFluxoDePotenciaFundamental;
import view.TelaHarmonico;

/**
 *
 * @author gdeusdara
 */
public class ControllerDistorcaoHarmonica {
    private DistorcaoHarmonica modelo;
     private javax.swing.JLabel equacao;
    private view.Grafico grafico;
    private javax.swing.JTabbedPane harmonicos;
    private javax.swing.JButton jButton1;
    private view.ModeloOnda ondaFundamental;
    private javax.swing.JComboBox<String> ordensHarmonicas;
    private javax.swing.JComboBox<String> paridade;
        
    /**
     *
     * @param equacao
     * @param grafico
     * @param harmonicos
     * @param jButton1
     * @param ondaFundamental
     * @param ordensHarmonicas
     * @param paridade
     */
    public ControllerDistorcaoHarmonica(JLabel equacao, Grafico grafico, JTabbedPane harmonicos, JButton jButton1, ModeloOnda ondaFundamental, JComboBox<String> ordensHarmonicas, JComboBox<String> paridade){
        modelo = new DistorcaoHarmonica();
        this.equacao = equacao;
        this.grafico = grafico;
        this.harmonicos = harmonicos;
        this.jButton1 = jButton1;
        this.ondaFundamental = ondaFundamental;
        this.paridade = paridade;
        this.ordensHarmonicas = ordensHarmonicas;
    }

    public void harmonicosStateChanged(ChangeEvent evt) {
        // TODO add your handling code here:
    }

    public void sairActionPerformed(ActionEvent evt, TelaDistorcaoHarmonica telaDistorcaoHarmonica) {
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            telaDistorcaoHarmonica.setVisible(false);
        } catch (Throwable ex) {
            Logger.getLogger(TelaFluxoDePotenciaFundamental.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
        modelo.clearHarmonicos();
        modelo.addHarmonicos(ordensHarmonicas.getSelectedIndex());
        harmonicos.removeAll();
        for (int i = 0; i < modelo.getNumeroDeHarmonicos(); i++) {
            harmonicos.addTab("Harmonico ".concat(String.valueOf(i + 1)), new TelaHarmonico(modelo.getHarmonico(i), paridade.getSelectedIndex()));
        }
    }

    public void simularActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
        modelo.setOndaFundamental(ondaFundamental.getModelo());
        grafico.construirOnda(modelo.getOndaResultante());
        equacao.setText(modelo.getEquacao());
    }

    public void ordensHarmonicasActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }
    
}
