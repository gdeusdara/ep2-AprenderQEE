/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gdeusdara
 */
public class DistorcaoHarmonica implements Simulacao {
    private OndaComum ondaFundamental;
    private List<Harmonico> harmonicos;
    
    public DistorcaoHarmonica(){
        ondaFundamental = new OndaComum();
        harmonicos = new ArrayList<>();
    }
    
    public void setOndaFundamental(OndaComum ondaFundamental){
        this.ondaFundamental = ondaFundamental;
    }
    
    public void addHarmonicos(int qtd){
        for(int i = 0; i < qtd; i++)
            harmonicos.add(new Harmonico());
    }
    
    public int getNumeroDeHarmonicos(){
        return harmonicos.size();
    }
    
    public void clearHarmonicos(){
        harmonicos.clear();
    }
    
    /**
     *
     * @return
     */
    @Override
    public List<Double> getOndaResultante(){
        double ponto;
	List<Double> pontosGrafico;
        pontosGrafico = new ArrayList<>();
		
	for(double t = 0; t<100; t++) {
                ponto = ondaFundamental.calculoOnda(t);
            for(int x = 0; x < harmonicos.size(); x++)
                ponto += harmonicos.get(x).calculoOnda(t);
            pontosGrafico.add(ponto);
	}
	return pontosGrafico;
    }
    
    public String getEquacao(){
        String equacao;
        equacao = "v(t) = " + String.valueOf(ondaFundamental.getAmplitude()) + "cos( wt + " +String.valueOf(ondaFundamental.getAngulo()) + ")";
        
        for(int i = 0; i < getNumeroDeHarmonicos(); i++){
            equacao += " + " + String.valueOf(getHarmonico(i).getAmplitude());
            equacao += "cos(" + String.valueOf(getHarmonico(i).getOrdemHarmonica()) + "wt + " + String.valueOf(getHarmonico(i).getAngulo()) + ")";
        }
        
        return equacao;
    }
    
    public Harmonico getHarmonico(int i){
        return harmonicos.get(i);
    }
}
