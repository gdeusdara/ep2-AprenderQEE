/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author gdeusdara
 */
public class Harmonico extends OndaComum {
    protected int ordemHarmonica;
    
    /**
     * @return the ordemHarmonica
     */
    public int getOrdemHarmonica() {
        return ordemHarmonica;
    }

    /**
     * @param ordemHarmonica the ordemHarmonica to set
     */
    public void setOrdemHarmonica(int ordemHarmonica) {
        this.ordemHarmonica = ordemHarmonica;
    }
    
    @Override
    public double calculoOnda(double t){
        return getAmplitude() * Math.cos(Math.toRadians( getOrdemHarmonica() * (FREQUENCIA_ANGULAR * t) + getAngulo()));
    }
    
}
