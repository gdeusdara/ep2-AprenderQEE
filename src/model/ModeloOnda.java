/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author gdeusdara
 */
public interface ModeloOnda {
    public final double FREQUENCIA_ANGULAR = 120 * Math.PI;
    public final int MAX_ANGULO = 180;
    public final int MIN_ANGULO = -180;
    
    public List<Double> getOnda ();
    
    
    public double calculoOnda (double t);
    
}
