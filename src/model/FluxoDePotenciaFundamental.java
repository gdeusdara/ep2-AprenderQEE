/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import view.ModeloTela;

/**
 *
 * @author gdeusdara
 */
public class FluxoDePotenciaFundamental implements Simulacao {
    
    public OndaComum tensao;
    public OndaComum corrente;
    
    public final double MAX_TENSAO = 220;
    public final double MIN_TENSAO = 0;
    public final double MAX_CORRENTE = 100;
    public final double MIN_CORRENTE = 0;
    
    public FluxoDePotenciaFundamental(OndaComum tensao, OndaComum corrente){
        this.tensao = tensao;
        this.corrente = corrente;
    }
    
    public double getPotenciaAtiva(){
        return tensao.getAmplitude() * corrente.getAmplitude() * Math.cos(Math.toRadians(tensao.getAngulo() - corrente.getAngulo()));
    }
    
    public double getPotenciaReativa(){
        return tensao.getAmplitude() * corrente.getAmplitude() * Math.sin(Math.toRadians(tensao.getAngulo() - corrente.getAngulo()));
    }
    
    public double getPotenciaAparente(){
        return tensao.getAmplitude() * corrente.getAmplitude();
    }
    
    public double getfatorDePotencia(){
        return Math.cos(Math.toRadians(tensao.getAngulo() - corrente.getAngulo()));
              
    }
    
    /**
     *
     * @return
     */
    @Override
    public List<Double> getOndaResultante (){
        double ponto;
	List<Double> pontosGrafico;
        pontosGrafico = new ArrayList<>();
		
	for(double t = 0; t<100; t++) {
            ponto = tensao.calculoOnda(t) * corrente.calculoOnda(t);
            pontosGrafico.add(ponto);
	}
	return pontosGrafico;
    }
}
