/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gdeusdara
 */
public class OndaComum implements ModeloOnda {
    double amplitude;
    double angulo;
    
    
    public OndaComum(){
        amplitude = 0;
        angulo = 0;
    }
    /**
     * @return the amplitude
     */
    public double getAmplitude() {
        return amplitude;
    }

    /**
     * @param amplitude the amplitude to set
     */
    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    /**
     * @return the angulo
     */
    public double getAngulo() {
        return angulo;
    }

    /**
     * @param angulo the angulo to set
     */
    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

   
    @Override
    public List<Double> getOnda (){
        double ponto;
	List<Double> pontosGrafico;
        pontosGrafico = new ArrayList<>();
		
	for(double t = 0; t<100; t++) {
            ponto = calculoOnda(t);
            pontosGrafico.add(ponto);
	}
	return pontosGrafico;
    }
    
    /**
     *
     * @param t
     * @return
     */
    @Override
    public double calculoOnda (double t){
        return getAmplitude() * Math.cos(Math.toRadians((FREQUENCIA_ANGULAR * t) + getAngulo()));
    }
    
}

