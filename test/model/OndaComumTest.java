/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gdeusdara
 */
public class OndaComumTest {
    
    public OndaComumTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAmplitude method, of class OndaComum.
     */
    @Test
    public void testGetAmplitude() {
        System.out.println("getAmplitude");
        OndaComum instance = new OndaComum();
        double expResult = 220;
        instance.setAmplitude(220);
        double result = instance.getAmplitude();
        assertEquals(expResult, result, 0.1);
        
    }

  

    /**
     * Test of getAngulo method, of class OndaComum.
     */
    @Test
    public void testGetAngulo() {
        System.out.println("getAngulo");
        OndaComum instance = new OndaComum();
        double expResult = 5;
        instance.setAngulo(5);
        double result = instance.getAngulo();
        assertEquals(expResult, result, 0.1);
      
    }

   
    

    /**
     * Test of calculoOnda method, of class OndaComum.
     */
    @Test
    public void testCalculoOnda() {
        System.out.println("calculoOnda");
        double t = 0;
        OndaComum instance = new OndaComum();
        instance.setAmplitude(1);
        instance.setAngulo(Math.PI);
        double expResult = 1;
        double result = instance.calculoOnda(t);
        assertEquals(expResult, result, 0.1);
    }
    
}
