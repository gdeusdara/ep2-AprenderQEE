/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gdeusdara
 */
public class FluxoDePotenciaFundamentalTest {
        
        OndaComum tensao;
        OndaComum corrente;
   
    public FluxoDePotenciaFundamentalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPotenciaAtiva method, of class FluxoDePotenciaFundamental.
     */
    @Test
    public void testGetPotenciaAtiva() {
        System.out.println("getPotenciaAtiva");
        FluxoDePotenciaFundamental instance1;
        instance1 = new FluxoDePotenciaFundamental(tensao, corrente);
        instance1.tensao.setAngulo(0);
        instance1.tensao.setAmplitude(220);
        instance1.corrente.setAngulo(35); 
        instance1.corrente.setAmplitude(39);        
        double expResult = 7028;
        double result = instance1.getPotenciaAtiva();
        assertEquals(expResult, result, 0.1);
    }

    
    @Test
    public void testGetPotenciaReativa() {
        System.out.println("getPotenciaReativa");
        FluxoDePotenciaFundamental instance1 = new FluxoDePotenciaFundamental(tensao, corrente);
        instance1.tensao.setAngulo(0);
        instance1.tensao.setAmplitude(220);
        instance1.corrente.setAngulo(35); 
        instance1.corrente.setAmplitude(39);
        double expResult = -4921;
        double result = instance1.getPotenciaReativa();
        assertEquals(expResult, result, 1);
    }

   
    /**
     * Test of getfatorDePotencia method, of class FluxoDePotenciaFundamental.
     */
    @Test
    public void testGetfatorDePotencia() {
        System.out.println("getfatorDePotencia");
        FluxoDePotenciaFundamental instance1 = new FluxoDePotenciaFundamental(tensao, corrente);
        instance1.tensao.setAngulo(0);
        instance1.tensao.setAmplitude(220);
        instance1.corrente.setAngulo(35); 
        instance1.corrente.setAmplitude(39);
        
        double expResult = 0.82;
        double result = instance1.getfatorDePotencia();
        assertEquals(expResult, result, 0.01);
        // TODO review the generated test code and remove the default call to fail.
    }

    
}
